STAR WARS SWAPI

Проект из учебной программы DanIt с использованием сервера колледжа https://ajax.test-danit.com/api/swapi.
На главной странице отображается список фильмов, а на странице одного фильма можно просмотреть всю необходимую информацию, которая отображается в табах.

Ипользованы технологии:
react
react-router-dom
react-redux (duck-style)
axios
styled-components
react-tabs
prop-types
moment
