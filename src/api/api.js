import { FetchData } from './base'
import axios from 'axios'

const api = new FetchData('https://ajax.test-danit.com/api/swapi')

export const getFilms = () => api.get('/films')
export const getOneFilm = (id) => api.get(`/films/${id}`)
export const getBatchData = (urls, cb) =>
  axios.all(urls.map((oneString) => axios.get(oneString))).then((respAll) => {
    if (respAll.length > 0) {
      const neededData = respAll
        .filter((obj) => obj.status === 200)
        .map((obj) => obj.data)
      cb(neededData)
    }
  })

export const getReturnBatchData = (urls) =>
  axios.all(urls.map((oneString) => axios.get(oneString))).then((respAll) => {
    if (respAll.length > 0) {
     return respAll
        .filter((obj) => obj.status === 200)
        .map((obj) => obj.data)
    }
  })

export const getItemByCategory = (category, id) => api.get(`/${category}/${id}`)

