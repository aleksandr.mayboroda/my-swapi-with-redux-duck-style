import React from 'react'
import styled from 'styled-components'

const StyledButton = styled.button`
  text-transform: capitalize;
  font-weight: bold;
  font-size: 1.5rem;
  outline: none;
  color: #fff;
  border-radius: 7px;
  border: 1px solid #fff;
  padding: 10px 20px;
  margin-top: 20px;
  background-color: transparent;
  transition: all ease-in-out 0.3s;
  cursor: pointer;

  &:hover {
    color: #282c34;
    background-color: #fff;
  }
`

const Button = ({ children, sx = {}, onClick }) => {
  // const wholeStyles = {
  //   textTransform: 'capitalize',
  //   fontWeight: 'bold',
  //   fontSize: '1.5rem',
  //   outline: 'none',
  //   cursor: 'pointer',
  //   ...sx
  // }
  return (
    <StyledButton style={sx} onClick={onClick}>
      {children}
    </StyledButton>
  )
}

export default Button
