import {useEffect, useState} from 'react'
import {useParams} from 'react-router-dom'
import styled from 'styled-components'
import {getOneFilm, getBatchData} from '../api/api'

import Button from './Button'

const WrapperGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);

  @media (min-width: 767px) {
    grid-template-columns: 1fr 3fr;
  }
`

const InfoBlock = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  border-radius: 7px;
  border: 1px solid #fff;
  padding: 10px;
`

const FilmCategoryInfo = () => {
  const [data, setData] = useState(null)
  const [categoryList, setCategoryList] = useState([])

  const {id, category} = useParams()

  const loadFilm = async () => {
    if (id) {
      const res = await getOneFilm(id)
      if (!res.isError) {
        setData(res.data)
        console.log(111,res.data)
      }
    }
    // setIsLoading(false)
  }
  
  const getCategoryLinks = async () => {
    // console.log(222, data[category])
    if (data[category] && data[category].length > 0) {
      await getBatchData(data[category], setCategoryList)
    }
  }

  useEffect(() => {
    loadFilm()
    return () => {
      setData(null)
    }
  }, [id, category])

  useEffect(() => {
    if(data)
    {
      getCategoryLinks()
    }
    return () => setCategoryList([])
  },[data])

  

  //запрос данных из фильма
  //отображение кнопок категории (название)
  //отображение выбранной единицы категории (вся информация + картинка сверху)

  console.log(id, category);
  return (
    <WrapperGrid>
       <InfoBlock>
          {categoryList.map((char) => (
            <Button key={char.id}
              sx={{
                fontSize: '1rem',
              }}
            >
              {char.name}
            </Button>
          ))}
        </InfoBlock>
      <div>
        {data && (
          <h1> Name</h1>
        )}
        {!data && (<h1>No data loaded</h1>)}
      </div>
    </WrapperGrid>
  )
}

export default FilmCategoryInfo
