import { useState, useEffect, memo } from 'react'
import styled from 'styled-components'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import InfoTabSecondary from './InfoTabSecondary'

import { useSelector, useDispatch } from 'react-redux'
import { filmsSelectors, filmsOperations } from '../store/films'

import LoaderLocal from './LoaderLocal'

const StyledTabs = styled(Tabs)`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  gap: 1rem;
  padding-top: 1rem;
  padding-bottom: 1rem;

  @media (min-width: 767px) {
    grid-template-columns: 1fr 3fr;
  }
`

const StyledTabList = styled(TabList)`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  list-style: none;

  @media (min-width: 767px) {
    flex-direction: column;
    align-items: stretch;
  }
`
const StyledTab = styled(Tab)`
  text-transform: capitalize;
  font-weight: bold;
  font-size: 1.5rem;
  outline: none;
  color: #fff;
  border-radius: 7px;
  border: 1px solid #fff;
  padding: 10px 20px;
  margin-bottom: 10px;
  background-color: transparent;
  transition: all ease-in-out 0.3s;
  cursor: pointer;

  &:hover {
    color: #282c34;
    background-color: #fff;
  }

  &.react-tabs__tab--selected {
    color: #282c34;
    background-color: #fff;
  }
`

const StyledTabPanel = styled(TabPanel)`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  gap: 1rem;

  // @media (min-width: 767px) {
  //   grid-template-columns: 2fr 3fr;
  // }
`

const categoriesList = [
  'characters',
  'planets',
  'starships',
  'vehicles',
  'species',
]

const InfoTabs = ({ filmData }) => {
  const [mainTabIndex, setMainTabIndex] = useState(0)
  const [existedCategoriesList, setExistedCategoriesList] = useState([])
  const isLoading = useSelector(filmsSelectors.getOneFilmIsLoading())

  const dispatch = useDispatch()
  const data = useSelector(filmsSelectors.getCurrentFilmCategories())
  console.log('rerender InfoTabs')
  const getCategoryLinks = async (category) => {
    if (!data || !data[category] || !data[category].length) {
      dispatch(
        filmsOperations.getFilmCategoryData(category, filmData[category])
      )
    }
  }

  const onSelect = async (index, _, ev) => {
    const category = ev.target.dataset.category
    await getCategoryLinks(category)
    setMainTabIndex(index)
  }

  //загрузка данных первой категории (которая есть)
  useEffect(() => {
    const filmsCategories = categoriesList.filter(
      (category) => category in filmData && filmData[category].length > 0
    )
    if (filmsCategories.length > 0) {
      setExistedCategoriesList(filmsCategories)
      getCategoryLinks(filmsCategories[mainTabIndex]) //0 as default
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <StyledTabs selectedIndex={mainTabIndex} onSelect={onSelect}>
      <StyledTabList>
        {existedCategoriesList.length > 0 &&
          existedCategoriesList.map((category, index) => (
            <StyledTab key={category + index} data-category={category}>
              {category}
            </StyledTab>
          ))}
      </StyledTabList>
      {isLoading && <LoaderLocal />}

      {existedCategoriesList.length > 0 && (
        <div>
          {existedCategoriesList.map((category, index) => (
            <StyledTabPanel key={category + index}>
              {data && data[category] && data[category].length > 0 && (
                <InfoTabSecondary
                  key={category + index}
                  category={category}
                  arrayOfButtons={data[category]}
                />
              )}
            </StyledTabPanel>
          ))}
        </div>
      )}
    </StyledTabs>
  )
}

export default memo(InfoTabs)
