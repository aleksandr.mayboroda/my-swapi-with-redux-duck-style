import React from 'react'
import moment from 'moment'

const FilmPageInfo = ({
  name,
  episodeId,
  director,
  producer,
  releaseDate,
  openingCrawl,
}) => {
  return (
    <>
      <h1>{name}</h1>
      <h5>Episode {episodeId}</h5>
      <h4>Director {director}</h4>
      <h5>{producer && producer.split(',').length > 0 ? 'Producers' : 'Producer'} {producer}</h5>
      <h6>Date of release: {moment(releaseDate).format('do MMMM Y')}</h6>
      <p>{openingCrawl}</p>
    </>
  )
}

export default FilmPageInfo
