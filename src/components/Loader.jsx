import React from 'react'
import styled from 'styled-components'

const StyledWrapper = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background:rgba(255,255,255,.9);
  display: flex;
  justify-content: center;
  align-items: center;
`

const StyledText = styled.h2`
  text-transform: uppercase;
  color: #000;
  font-size: 3rem;
`

const Loader = () => {
  return (
    <StyledWrapper>
      <StyledText>
        loading...
      </StyledText>
    </StyledWrapper>
  )
}

export default Loader
