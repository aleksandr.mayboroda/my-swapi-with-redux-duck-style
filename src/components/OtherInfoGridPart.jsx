import React from 'react'
import styled from 'styled-components'
import {Link} from 'react-router-dom'
import Button from './Button'

const InfoBlock = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  border-radius: 7px;
  border: 1px solid #fff;
  padding: 10px;
`

const StyledLink = styled(Link)`
  text-decoration: none;
  padding: 5px 10px;
  border-radius: 7px;
  border: 1px solid #fff;
  background-color: transparent;
  color: #fff;
  margin: 5px;
  transition: all ease-in-out .3s;

  &:hover{
    color: #282c34;
    background-color: #fff;
  }
`

const OtherInfoGridPart = ({loadFunct, buttonText, headerText, arrayToIterate, linkResourse = '' }) => {
  return (
    <div>
    {!arrayToIterate.length && (
      <Button
      onClick={() => loadFunct()}
      >{buttonText}</Button>
    )}
    {arrayToIterate.length > 0 && (
      <>
        <h2>{headerText}:</h2>
        <InfoBlock>
          {arrayToIterate.map((char) => (
            <StyledLink key={char.id} to={`${linkResourse}/${char.id}`}>
              {char.name}
            </StyledLink>
          ))}
        </InfoBlock>
      </>
    )}
  </div>
  )
}

export default OtherInfoGridPart
