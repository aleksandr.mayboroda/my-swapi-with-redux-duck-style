import styled from 'styled-components'
import picture from '../../img/spacies.jpg'

const StyledWrapper = styled.div`
  border-radius: 7px;
  border: 1px solid #fff;
  padding: 20px;
`
const CharImage = styled.img.attrs({ src: picture, alt: 'char-picture' })`
  width: 100%;
  height: 250px;
  object-fit: cover;
`

const Text = styled.p`
  text-transform: capitalize;
`

const SubText = styled.span`
  color: #ffffff70;
  text-transform: lowercase;
  margin-right: 5px;
`

const Spacies = ({data}) => {
  return (
    <StyledWrapper>
    <div>
      <CharImage />
    </div>
    {data?.name && <h2>{data.name}</h2>}
    {data?.classification && <Text><SubText>classification:</SubText>{data.classification}</Text>}
    {data?.language && <Text><SubText>language:</SubText>{data.language}</Text>}
    {data?.averageHeight && <Text><SubText>average height:</SubText>{data.averageHeight}</Text>}
    {data?.averageLifespan && <Text><SubText>average lifespan:</SubText>{data.averageLifespan}</Text>}
    {data?.skinColors && <Text><SubText>skin colors:</SubText>{data.skinColors}</Text>}
    {data?.hairColors && <Text><SubText>hair colors:</SubText>{data.hairColors}</Text>}
    {data?.eyeColors && <Text><SubText>eye colors:</SubText> {data.eyeColors}</Text>}
  </StyledWrapper>
  )
}

export default Spacies
