import styled from 'styled-components'
import picture from '../../img/yoda.jpg'

const StyledWrapper = styled.div`
  border-radius: 7px;
  border: 1px solid #fff;
  padding: 20px;
`
const CharImage = styled.img.attrs({ src: picture, alt: 'char-picture' })`
  width: 100%;
  height: 250px;
  object-fit: cover;
`

const Text = styled.p`
  text-transform: capitalize;
`

const SubText = styled.span`
  color: #ffffff70;
  text-transform: lowercase;
  margin-right: 5px;
`

const Character = ({ data }) => {
  return (
    <StyledWrapper>
      <div>
        <CharImage />
      </div>
      {data?.name && <h2>{data.name}</h2>}
      {data?.birthYear && <Text><SubText>birth year:</SubText>{data.birthYear}</Text>}
      {data?.gender && <Text><SubText>gender:</SubText>{data.gender}</Text>}
      {data?.height && <Text><SubText>height:</SubText>{data.height}</Text>}
      {data?.mass && <Text><SubText>mass:</SubText>{data.mass}</Text>}
      {data?.skinColor && <Text><SubText>skin color:</SubText>{data.skinColor}</Text>}
      {data?.hairColor && <Text><SubText>hair color:</SubText>{data.hairColor}</Text>}
      {data?.eyeColor && <Text><SubText>eye color:</SubText> {data.eyeColor}</Text>}
    </StyledWrapper>
  )
}

export default Character
