import styled from 'styled-components'
import picture from '../../img/planet.jpg'

const StyledWrapper = styled.div`
  border-radius: 7px;
  border: 1px solid #fff;
  padding: 20px;
`
const CharImage = styled.img.attrs({ src: picture, alt: 'char-picture' })`
  width: 100%;
  height: 250px;
  object-fit: cover;
`

const Text = styled.p`
  text-transform: capitalize;
`

const SubText = styled.span`
  color: #ffffff70;
  text-transform: lowercase;
  margin-right: 5px;
`

const Planet = ({data}) => {
  return (
    <StyledWrapper>
      <div>
        <CharImage />
      </div>
      {data?.name && <h2>{data.name}</h2>}
      {data?.terrain && <Text><SubText>terrain:</SubText>{data.terrain}</Text>}
      {data?.climate && <Text><SubText>climate:</SubText>{data.climate}</Text>}
      {data?.gravity && <Text><SubText>gravity:</SubText>{data.gravity}</Text>}
      {data?.surfaceWater && <Text><SubText>surface water:</SubText>{data.surfaceWater}</Text>}
      {data?.population && <Text><SubText>population:</SubText>{data.population}</Text>}
      {data?.diameter && <Text><SubText>diameter:</SubText>{data.diameter}</Text>}
      {data?.orbitalPeriod && <Text><SubText>orbital period:</SubText> {data.orbitalPeriod}</Text>}
      {data?.rotationPeriod && <Text><SubText>rotation period:</SubText> {data.rotationPeriod}</Text>}
    </StyledWrapper>
  )
}

export default Planet
