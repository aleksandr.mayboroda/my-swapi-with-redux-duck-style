import styled from 'styled-components'
import picture from '../../img/vehicle.jpg'

const StyledWrapper = styled.div`
  border-radius: 7px;
  border: 1px solid #fff;
  padding: 20px;
`
const Image = styled.img.attrs({ src: picture, alt: 'char-picture' })`
  width: 100%;
  height: 250px;
  object-fit: cover;
`

const Text = styled.p`
  text-transform: capitalize;
`

const SubText = styled.span`
  color: #ffffff70;
  text-transform: lowercase;
  margin-right: 5px;
`
const Vehicle = ({data}) => {
  return (
    <StyledWrapper>
    <div>
      <Image />
    </div>
    {data?.name && <h2>{data.name}</h2>}
    {data?.model && <Text><SubText>model:</SubText>{data.model}</Text>}
    {data?.manufacturer && <Text><SubText>manufacturer:</SubText>{data.manufacturer}</Text>}
    {data?.vehicleClass && <Text><SubText>vehicle class:</SubText>{data.vehicleClass}</Text>}
    {data?.costInCredits && <Text><SubText>cost:</SubText>{data.costInCredits }</Text>}
    {data?.crew > 0 && <Text><SubText>crew :</SubText>{data.crew }</Text>}
    {data?.passengers > 0 && <Text><SubText>passengers  :</SubText>{data.passengers  }</Text>}
    {data?.consumables && <Text><SubText>consumables:</SubText>{data.consumables}</Text>}
    {data?.length && <Text><SubText>length:</SubText> {data.length}</Text>}
    {data?.cargoCapacity > 0 && <Text><SubText>cargoCapacity:</SubText> {data.cargoCapacity}</Text>}
    {data?.hyperdriveRating && <Text><SubText>hyper drive rating:</SubText> {data.hyperdriveRating}</Text>}
    {data?.maxAtmosphericSpeed && <Text><SubText>max atmospheric speed:</SubText> {data.maxAtmosphericSpeed}</Text>}
    {data?.mglt && <Text><SubText>mglt:</SubText> {data.mglt}</Text>}
  </StyledWrapper>
  )
}

export default Vehicle
