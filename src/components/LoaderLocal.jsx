import React from 'react'
import styled from 'styled-components'

const StyledWrapper = styled.div`
  width: 100%;
  hieght: 100%;
  background: rgba(255, 255, 255, 0.9);
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: var(--border-rad);
`

const StyledText = styled.h2`
  text-transform: uppercase;
  color: #000;
  font-size: 3rem;
`

const LoaderLocal = () => {
  return (
    <StyledWrapper>
      <StyledText>loading...</StyledText>
    </StyledWrapper>
  )
}

export default LoaderLocal