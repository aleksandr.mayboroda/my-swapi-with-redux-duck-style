import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import moment from 'moment'

const CardWrapper = styled(Link)`
  position: relative;
  border-radius: var(--border-rad);
  border: var(--border);
  background-color: var(--bg-black);
  box-sizing: border-box;
  width: 100%;
  text-decoration: none;
  color: var(--text-white);

  &:hover {
    // border-bottom-left-radius: 0;
    // border-bottom-right-radius: 0;
    background-color: var(--bg-white);
  }
`
const PaddingBlock = styled.div`
  padding: 0.5rem;
`

const CartTitle = styled.h4``
const CartEpisode = styled.p``
const CartDirector = styled.h5``
const CartProducer = styled.h6``
const CartDate = styled.p``
const CartDescription = styled.p`
  border-radius: var(--border-rad);
  margin: 0;
  padding: 1rem;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  overflow: hidden;
  transition: var(--trans-all);
  color: var(--bg-black);
  position: absolute;
  z-index: 2;
`

const FilmCard = ({
  id,
  name,
  episodeId,
  director,
  producer,
  releaseDate,
  openingCrawl,
}) => {
  const [showAdditional, setShowAdditional] = useState(false)
  return (
    <CardWrapper
      to={`/film/${id}`}
      onMouseEnter={() => setShowAdditional(true)}
      onMouseLeave={() => setShowAdditional(false)}
    >
      <PaddingBlock>
        <CartTitle>{name}</CartTitle>
        <CartEpisode>Episode # {episodeId}</CartEpisode>
        <CartDirector>Director: {director}</CartDirector>
        <CartProducer>Producer: {producer}</CartProducer>
        <CartDate>
          Relesed: {moment(releaseDate, 'YYYYMMDD').fromNow()}
        </CartDate>
      </PaddingBlock>
      {showAdditional && <CartDescription>{openingCrawl}</CartDescription>}
      {/* <CartDescription>{openingCrawl}</CartDescription> */}
    </CardWrapper>
  )
}

export default FilmCard
