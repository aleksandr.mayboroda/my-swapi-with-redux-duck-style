import { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { filmsSelectors, filmsOperations } from '../store/films'

import styled from 'styled-components'
import Loader from './Loader'
import FilmCard from './FilmCard'

const ListWrapper = styled.div`
  padding: 1rem 4rem;
  display: grid;
  grid-template-columns: repeat(1,1fr);
  gap: 1rem;


@media(min-width: 767px)
{
  grid-template-columns: repeat(2,1fr);
}

@media(min-width: 1024px)
{
  grid-template-columns: repeat(4,1fr);
}

`

const FilmList = () => {
  const dispatch = useDispatch()
  const filmsList = useSelector(filmsSelectors.getFilms())
  const isLoading = useSelector(filmsSelectors.getIsLoading())

  useEffect(() => {
    if(!filmsList.length)
    {
      dispatch(filmsOperations.fetchFilms())
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if(isLoading)
  {
    return <Loader />
  }

  return (
    <ListWrapper>
        {filmsList.length > 0 && filmsList.map(film => <FilmCard key={film.id} {...film}/>)}
    </ListWrapper>
  )
}

export default FilmList
