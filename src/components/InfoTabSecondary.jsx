import { useState, useEffect, memo } from 'react'
import styled from 'styled-components'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import Character from './PartVuews/Character'
import Planet from './PartVuews/Planet'
import Ship from './PartVuews/Ship'
import Vehicle from './PartVuews/Vehicle'
import Spacies from './PartVuews/Spacies'

import { useSelector } from 'react-redux'
import { filmsSelectors } from '../store/films'

const StyledTabs = styled(Tabs)`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  gap: 1rem;

  @media (min-width: 767px) {
    grid-template-columns: 2fr 3fr;
  }
`

const StyledTabList = styled(TabList)`
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  list-style: none;

  @media (min-width: 767px) {
    justify-content: flex-start;
    align-content: flex-start;
  }
`

const StyledTab = styled(Tab)`
  text-transform: capitalize;
  font-weight: bold;
  font-size: 1rem;
  outline: none;
  color: #fff;
  border-radius: var(--border-rad);
  border: 1px solid #fff;
  padding: 5px 10px;
  margin: 5px;
  background-color: transparent;
  transition: all ease-in-out 0.3s;
  cursor: pointer;

  &:hover {
    color: #282c34;
    background-color: #fff;
  }

  &.react-tabs__tab--selected {
    color: #282c34;
    background-color: #fff;
  }
`

const InfoTabSecondary = ({ category, arrayOfButtons }) => {
  const [activeTabIndex, setActiveTabIndex] = useState(0)
  const [data, setData] = useState({})
  const state = useSelector(filmsSelectors.getCurrentFilmCategories())
  console.log('rerender InfoTabSecondary')
  const onSelect = async (index, lol, ev) => {
    await getItemData(ev.target.dataset.id)
    setActiveTabIndex(index)
  }

  const getItemData = async (id) => {
    if (
      category &&
      id &&
      state &&
      state[category] &&
      state[category].find((item) => item.id === +id)
    ) {
      setData(state[category].find((item) => item.id === +id))
    }
  }

  //загрузка данных первой категории (которая есть)
  useEffect(() => {
    if (arrayOfButtons.length > 0) {
      getItemData(arrayOfButtons[0].id)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <StyledTabs selectedIndex={activeTabIndex} onSelect={onSelect}>
      <StyledTabList>
        {state &&
          state[category] &&
          state[category].length > 0 &&
          state[category].map((item) => (
            <StyledTab key={item.id} data-id={item.id}>
              {item.name}
            </StyledTab>
          ))}
      </StyledTabList>
      <div>
        {state &&
          state[category] &&
          state[category].length > 0 &&
          state[category].map((item) => (
            <TabPanel key={item.id}>
              {!data && <h2>Oh Shit!</h2>}
              {data && category === 'characters' && <Character data={data} />}
              {data && category === 'planets' && <Planet data={data} />}
              {data && category === 'starships' && <Ship data={data} />}
              {data && category === 'vehicles' && <Vehicle data={data} />}
              {data && category === 'species' && <Spacies data={data} />}
            </TabPanel>
          ))}
      </div>
    </StyledTabs>
  )
}

export default memo(InfoTabSecondary)
