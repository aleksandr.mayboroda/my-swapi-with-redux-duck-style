import reducer from './reducer'

export {default as filmsSelectors} from './selectors'
export {default as filmsOperations} from './operations'

export default reducer