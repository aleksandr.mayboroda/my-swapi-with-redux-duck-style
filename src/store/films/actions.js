import types from './types'

const setIsLoading = (value) => ({
  type: types.SET_IS_LOADING,
  payload: value,
})

const setFilms = (value) => ({
  type: types.SET_FILMS,
  payload: value,
})

const setOneFilm = (value) => ({
  type: types.SET_ONE_FILM,
  payload: value,
})

const setOneFilmCategories = (data) => ({
  type: types.SET_ONE_FILM_CATEGORIES,
  payload: data,
})

const setOneFilmIsLoading = (data) => ({
  type: types.SET_ONE_FILM_IS_LOADING,
  payload: data,
})


const def = {
  setIsLoading, setFilms, setOneFilm, setOneFilmCategories, setOneFilmIsLoading,
}

export default def