import types from './types'

const initialState = {
  data: [],
  oneFilm: {
    data: null,
    categories: null, //{characters: [{...}, ...], planets: [] ...}
    isLoading: false,
  },
  isLoading: true,
}

const reducer = (state = initialState, action) => {
  switch(action.type){
    case types.SET_FILMS : {
      return {...state, data: action.payload} //isLoading: false
    }
    case types.SET_ONE_FILM : {
      return {...state, oneFilm: {...state.oneFilm, data: action.payload}} //isLoading: false
    }
    case types.SET_ONE_FILM_CATEGORIES : {
      return {...state, oneFilm: {...state.oneFilm, categories: action.payload}} //isLoading: false
    }
    case types.SET_IS_LOADING : {
      return {...state, isLoading: action.payload}
    }
    case types.SET_ONE_FILM_IS_LOADING : {
      return {...state, oneFilm: {...state.oneFilm, isLoading: action.payload}}
    }
    default: 
      return state
  }
}

export default reducer