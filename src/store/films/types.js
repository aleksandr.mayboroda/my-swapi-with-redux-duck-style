const SET_FILMS = 'swapi-films/films/SET_FILMS'
const SET_ONE_FILM = 'swapi-films/films/SET_ONE_FILM'
const SET_ONE_FILM_CATEGORIES = 'swapi-films/films/SET_ONE_FILM_CATEGORIES'
const SET_IS_LOADING = 'swapi-films/films/SET_IS_LOADING'
const SET_ONE_FILM_IS_LOADING = 'swapi-films/films/SET_ONE_FILM_IS_LOADING'

const def = {
  SET_FILMS, SET_ONE_FILM, SET_ONE_FILM_CATEGORIES, SET_IS_LOADING, SET_ONE_FILM_IS_LOADING,
}

export default def