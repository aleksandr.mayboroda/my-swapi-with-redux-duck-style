import actions from './actions'
import { getFilms, getOneFilm, getReturnBatchData } from '../../api/api'

const fetchFilms = () => async (dispatch) => {
  dispatch(actions.setIsLoading(true))
  const result = await getFilms()
  if (!result.isError) {
    dispatch(actions.setFilms(result.data))
  } else {
    //подумать, как лучше. Может прикрутить
  }
  dispatch(actions.setIsLoading(false))
}

// eslint-disable-next-line no-undef
const fetchOneFilm = (id) => async (dispatch, getState) => {
  dispatch(actions.setIsLoading(true))
  const filmData = getState().films.data.filter((film) => film.id === id)
  if (filmData.length > 0) {
    dispatch(actions.setOneFilm(filmData[0]))
  } else {
    const res = await getOneFilm(id)
    if (!res.isError) {
      dispatch(actions.setOneFilm(res.data))
    }
  }

  dispatch(actions.setIsLoading(false))
}

const clearOneFilm = () => (dispatch) => {
  dispatch(actions.setOneFilm(null))
}

const getFilmCategoryData = (category, arrayOfLinks) => async (dispatch, getState) => {
  dispatch(actions.setOneFilmIsLoading(true))
  const result = await getReturnBatchData(arrayOfLinks)
  if(!getState().films.oneFilm.categories )
  {
    dispatch(actions.setOneFilmCategories({[category]: result}))
  }
  else
  {
    dispatch(actions.setOneFilmCategories({...getState().films.oneFilm.categories, [category]: result}))
  }
  dispatch(actions.setOneFilmIsLoading(false))
}

const def = {
  fetchFilms,
  fetchOneFilm,
  clearOneFilm,
  getFilmCategoryData,
}

export default def
