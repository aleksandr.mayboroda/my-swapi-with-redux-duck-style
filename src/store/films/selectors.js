const getFilms = () => state => state.films.data
const getCurrentFilm = () => state => state.films.oneFilm.data
const getIsLoading = () => state => state.films.isLoading
const getCurrentFilmCategories = () => state => state.films.oneFilm.categories
const getOneFilmIsLoading = () => state => state.films.oneFilm.isLoading
const def = {
  getFilms, getCurrentFilm, getIsLoading, getCurrentFilmCategories, getOneFilmIsLoading,
}

export default def