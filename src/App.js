import './App.css'
import AppRoutes from './routes/AppRoutes'

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <p>Swapi films</p>
        <AppRoutes />
      </header>
    </div>
  )
}

export default App
