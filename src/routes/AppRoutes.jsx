import React from 'react'
import { Routes, Route } from 'react-router-dom'
import Main from '../pages/Main'
import FilmPage from '../pages/FilmPage'
import FilmPageComplicated from '../pages/FilmPageComplicated'
// import FilmCategoryInfo from '../components/FilmCategoryInfo'

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<Main />} />
      <Route path="films/:id" element={<FilmPage />} />
      <Route path="film/:id" element={<FilmPageComplicated />}>
        {/* <Route
          index
          element={
            <div>
              <h2>Select category</h2>
            </div>
          }
        />
        <Route path={':category'} element={<FilmCategoryInfo />} />
        <Route
          path="*"
          element={
            <div style={{ padding: '1rem' }}>
              <h2>There's nothing here!</h2>
            </div>
          }
        /> */}
      </Route>
    </Routes>
  )
}

export default AppRoutes
