import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import styled from 'styled-components'
import { getOneFilm, getBatchData } from '../api/api'
import picture from '../img/film-image.jpg'
// import Loader from '../components/Loader'
import OtherInfoGridPart from '../components/OtherInfoGridPart'

// import axios from 'axios'

import FilmPageInfo from '../components/FilmPageInfo'

const PageWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px;
`

const MainInfoGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  gap: 1rem;

  @media (min-width: 767px) {
    grid-template-columns: repeat(2, 1fr);
  }

  // @media (min-width: 1024px) {
  //   grid-template-columns: repeat(4, 1fr);
  // }
`

const InfoImage = styled.img.attrs({ src: picture, alt: 'sw-picture' })`
  width: 100%;
  height: 100%;
  object-fit: cover;
`

const OtherInfoGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  gap: 1rem;

  @media (min-width: 767px) {
    grid-template-columns: repeat(2, 1fr);
  }
`

const FilmPage = () => {
  const { id } = useParams()
  const [filmData, setFilmData] = useState(null)
  const [characters, setCharacters] = useState([])
  const [planets, setPlanets] = useState([])
  const [starships, setStarships] = useState([])
  const [vehicles, setVehicles] = useState([])
  const [species, setSpecies] = useState([])
  // const [isLoading, setIsLoading] = useState(true)

  const loadFilms = async () => {
    if (id) {
      const res = await getOneFilm(id)
      if (!res.isError) {
        setFilmData(res.data)
      }
    }
    // setIsLoading(false)
  }
  console.log('bbb', filmData, id)

  const getCharacters = async () => {
    if (filmData?.characters.length > 0) {
      await getBatchData(filmData.characters, setCharacters)
    }
  }

  const getPlanets = async () => {
    if (filmData?.planets.length > 0) {
      await getBatchData(filmData.planets,setPlanets)
    }
  }

  const getStarShips = async () => {
    if (filmData?.starships.length > 0) {
      await getBatchData(filmData.starships,setStarships)
    }
  }

  const getVehicles = async () => {
    if (filmData?.vehicles.length > 0) {
      await getBatchData(filmData.vehicles,setVehicles)
    }
  }

  const getSpecies = async () => {
    if (filmData?.species.length > 0) {
      await getBatchData(filmData.species,setSpecies)
    }
  }

  useEffect(() => {
    loadFilms()
    return () => {
      setFilmData(null)
      // setIsLoading(true)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <PageWrapper>
      <div>
        <MainInfoGrid>
          <div>
            <InfoImage />
          </div>
          <div>
            {filmData && <FilmPageInfo {...filmData} key={filmData.id} />}
          </div>
        </MainInfoGrid>
        <OtherInfoGrid>
          {filmData?.characters.length > 0 && (<OtherInfoGridPart 
            loadFunct={getCharacters}
            buttonText="Show Characters"
            headerText="Characters"
            arrayToIterate={characters}
            linkResourse='/characters'
          />)}
          {filmData?.planets.length > 0 && (<OtherInfoGridPart 
            loadFunct={getPlanets}
            buttonText="Show Planets"
            headerText="Planets"
            arrayToIterate={planets}
            linkResourse='/planets'
          />)}
           {filmData?.starships.length > 0 && (<OtherInfoGridPart 
            loadFunct={getStarShips}
            buttonText="Show Starships"
            headerText="Starships"
            arrayToIterate={starships}
            linkResourse='/starships'
          />)}
           {filmData?.vehicles.length > 0 && (<OtherInfoGridPart 
            loadFunct={getVehicles}
            buttonText="Show Vehicles"
            headerText="Vehicles"
            arrayToIterate={vehicles}
            linkResourse='/vehicles'
          />)}
          {filmData?.species.length > 0 && (<OtherInfoGridPart 
            loadFunct={getSpecies}
            buttonText="Show Species"
            headerText="Species"
            arrayToIterate={species}
            linkResourse='/species'
          />)}
        </OtherInfoGrid>
        <div>

        </div>
      </div>
    </PageWrapper>
  )
}

export default FilmPage
