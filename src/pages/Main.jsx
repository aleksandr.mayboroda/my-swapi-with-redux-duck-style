import React from 'react'
import FilmList from '../components/FilmList'

const Main = () => {
  return (
    <>
      <h1>Main page</h1>
      <FilmList />
    </>
  )
}

export default Main
