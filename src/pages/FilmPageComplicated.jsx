import React, { useEffect } from 'react'
import {useParams} from 'react-router-dom'
import styled from 'styled-components'
import picture from '../img/film-image.jpg'
import { useSelector, useDispatch } from 'react-redux'
import { filmsSelectors, filmsOperations } from '../store/films'
import Loader from '../components/Loader'
import FilmPageInfo from '../components/FilmPageInfo'
import InfoTabs from '../components/InfoTabs'

const PageWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px;
`

const MainInfoGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  gap: 1rem;

  @media (min-width: 767px) {
    grid-template-columns: repeat(2, 1fr);
  }

  // @media (min-width: 1024px) {
  //   grid-template-columns: repeat(4, 1fr);
  // }
`

const InfoImage = styled.img.attrs({ src: picture, alt: 'sw-picture' })`
  width: 100%;
  height: 100%;
  object-fit: cover;
`

const FilmPageComplicated = () => {
  const { id } = useParams()
  const dispatch = useDispatch()
  const filmData = useSelector(filmsSelectors.getCurrentFilm(id))
  const isLoading = useSelector(filmsSelectors.getIsLoading())

  useEffect(() => {
    if (id) {
      dispatch(filmsOperations.fetchOneFilm(id))
    }

    return () => {
      dispatch(filmsOperations.clearOneFilm())
      // setIsLoading(true)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if(isLoading)
  {
    return <Loader />
  }

  return (
    <PageWrapper>
      <div>
        <MainInfoGrid>
          <div>
            <InfoImage />
          </div>
          <div>
            {filmData && <FilmPageInfo {...filmData} key={filmData.id} />}
          </div>
        </MainInfoGrid>
        {filmData && (<InfoTabs filmData={filmData} />)}
      </div>
    </PageWrapper>
  )
}

export default FilmPageComplicated
